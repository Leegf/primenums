#ifndef LISTAT_PRIMESSEARCH_H
#define LISTAT_PRIMESSEARCH_H

#include "main.h"
#include <future>
#include <string>

using fut = std::future<void>;
using futVec = std::vector<fut>;

class PrimesSearch {
public:
	PrimesSearch();
	PrimesSearch(std::vector<std::pair<int, int>>);
	~PrimesSearch();

	void calculate(std::vector<std::pair<int, int>>);
	void output(std::string par = "output.xml");

	PrimesSearch(PrimesSearch const &src) = delete;
	PrimesSearch &operator=(PrimesSearch const &rhs) = delete;

private:
	futVec _futVec;
	std::vector<int> _res;
	std::mutex _m;
	void _searchSimpleNums(int low, int high);
	bool _isPrime(int num) const;
};

#endif //LISTAT_PRIMESSEARCH_H
