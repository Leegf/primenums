#include "ParseXML.h"
#include "PrimesSearch.h"
#include "gtest/gtest.h"

TEST (file, WrongFileNames)
{
	EXPECT_THROW(ParseXML("foo"), corrupted_file);
	EXPECT_THROW(ParseXML("bar"), corrupted_file);
	EXPECT_THROW(ParseXML(""), corrupted_file);
	EXPECT_THROW(ParseXML(".xml"), corrupted_file);
	EXPECT_THROW(ParseXML("   tests/file.xml"), corrupted_file);
	EXPECT_THROW(ParseXML("   tests/file      .xml"), corrupted_file);
}
TEST (format, WrongFormat)
{
	ParseXML tmp("tests/file2.xml");
	EXPECT_THROW(tmp.getRes(), wrong_format);
}

TEST (format, WrongFormat2)
{
	ParseXML tmp("tests/file3.xml");
	EXPECT_THROW(tmp.getRes(), wrong_format);
}

TEST (format, WrongFormat3)
{
	ParseXML tmp("tests/file4.xml");
	EXPECT_THROW(tmp.getRes(), wrong_format);
}

TEST (format, WrongFormat4)
{
	ParseXML tmp("tests/file5.xml");
	EXPECT_THROW(tmp.getRes(), wrong_format);
}

TEST (format, WrongFormat5)
{
	ParseXML tmp("tests/file6.xml");
	EXPECT_THROW(tmp.getRes(), std::exception);
}

TEST (format, WrongFormat6)
{
	ParseXML tmp("tests/file7.xml");
	EXPECT_THROW(tmp.getRes(), wrong_format);
}

TEST (format, WrongFormat7)
{
	ParseXML tmp("tests/file8.xml");
	EXPECT_THROW(tmp.getRes(), std::exception);
}

TEST (format, WrongFormat8)
{
	ParseXML tmp("tests/file9.xml");
	EXPECT_THROW(tmp.getRes(), wrong_format);
}

TEST (format, WrongFormat9)
{
	ParseXML tmp("tests/file10.xml");
	EXPECT_THROW(tmp.getRes(), wrong_format);
}

TEST (format, WrongFormat10)
{
	ParseXML tmp("tests/filew.xml");
	EXPECT_THROW(tmp.getRes(), wrong_format);
}

TEST (format, WrongFormat11)
{
	ParseXML tmp("tests/filew2.xml");
	EXPECT_THROW(tmp.getRes(), wrong_format);
}

TEST(output, PrimeCheck1)
{
	ParseXML tmp("tests/file.xml");
	PrimesSearch res(tmp.getRes());
	res.output();
	std::ifstream ifs("output.xml");
	std::stringstream ifss;
	ifss << ifs.rdbuf();
	std::string buf = ifss.str();
	ifs.close();

	std::smatch m;
	std::regex e(R"(<primes>\s*)");
	std::regex_search(buf, m, e);
	buf = m.suffix();

	std::istringstream iss(buf);
	std::vector<int> nums( (std::istream_iterator<int>(iss)), std::istream_iterator<int>() );
	std::sort(nums.begin(), nums.end());

	std::vector<int> chk = {11, 13, 17, 19, 53, 59, 61, 67};
	ASSERT_TRUE(nums == chk);
}

TEST(output, PrimeCheck2)
{
	ParseXML tmp("tests/file11v.xml");
	PrimesSearch res(tmp.getRes());
	res.output();
	std::ifstream ifs("output.xml");
	std::stringstream ifss;
	ifss << ifs.rdbuf();
	std::string buf = ifss.str();
	ifs.close();

	std::smatch m;
	std::regex e(R"(<primes>\s*)");
	std::regex_search(buf, m, e);
	buf = m.suffix();

	std::istringstream iss(buf);
	std::vector<int> nums( (std::istream_iterator<int>(iss)), std::istream_iterator<int>() );
	std::sort(nums.begin(), nums.end());

	std::vector<int> chk = {1, 2, 3, 5, 7};
	ASSERT_TRUE(nums == chk);
}

TEST(output, PrimeCheck3)
{
	ParseXML tmp("tests/file12v.xml");
	PrimesSearch res(tmp.getRes());
	res.output();
	std::ifstream ifs("output.xml");
	std::stringstream ifss;
	ifss << ifs.rdbuf();
	std::string buf = ifss.str();
	ifs.close();

	std::smatch m;
	std::regex e(R"(<primes>\s*)");
	std::regex_search(buf, m, e);
	buf = m.suffix();

	std::istringstream iss(buf);
	std::vector<int> nums( (std::istream_iterator<int>(iss)), std::istream_iterator<int>() );
	std::sort(nums.begin(), nums.end());

	std::vector<int> chk = {1, 2, 3, 5, 7, 53, 53, 83, 89};
	ASSERT_TRUE(nums == chk);
}

TEST(output, PrimeCheck4)
{
	ParseXML tmp("tests/file13v.xml");
	PrimesSearch res(tmp.getRes());
	res.output();
	std::ifstream ifs("output.xml");
	std::stringstream ifss;
	ifss << ifs.rdbuf();
	std::string buf = ifss.str();
	ifs.close();

	std::smatch m;
	std::regex e(R"(<primes>\s*)");
	std::regex_search(buf, m, e);
	buf = m.suffix();

	std::istringstream iss(buf);
	std::vector<int> nums( (std::istream_iterator<int>(iss)), std::istream_iterator<int>() );
	std::sort(nums.begin(), nums.end());

	std::vector<int> chk = {1, 2, 3, 5, 7, 53, 53, 83, 89, 211};
	ASSERT_TRUE(nums == chk);
}

TEST(output, PrimeCheck5)
{
	ParseXML tmp("tests/file14v.xml");
	PrimesSearch res(tmp.getRes());
	res.output();
	std::ifstream ifs("output.xml");
	std::stringstream ifss;
	ifss << ifs.rdbuf();
	std::string buf = ifss.str();
	ifs.close();

	std::smatch m;
	std::regex e(R"(<primes>\s*)");
	std::regex_search(buf, m, e);
	buf = m.suffix();

	std::istringstream iss(buf);
	std::vector<int> nums( (std::istream_iterator<int>(iss)), std::istream_iterator<int>() );
	std::sort(nums.begin(), nums.end());

	std::vector<int> chk = {1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199};
	ASSERT_TRUE(nums == chk);
}

int 	main(int ac, char **av) {
	::testing::InitGoogleTest(&ac, av);
	return (RUN_ALL_TESTS());
}