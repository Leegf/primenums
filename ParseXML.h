#ifndef LISTAT_PARSEXML_H
#define LISTAT_PARSEXML_H

#include <regex>
#include <sstream>
#include "main.h"
#include "SimpleLexer.h"

class ParseXML {
public:
	ParseXML();
	ParseXML(const char * fileName);
	~ParseXML();
	void setFile(const char * fileName);
	bool lexerCheck();
	std::vector<std::pair<int, int>> getRes();

private:
	SimpleLexer _lex;
	bool _flag = false;
	std::vector<std::pair<int, int>> _res;
	std::string _buff;
};

#endif //LISTAT_PARSEXML_H
