#include <iostream>
#include "ParseXML.h"
#include "PrimesSearch.h"

int		main(int ac, char **av)
{
	if (ac == 1) {
		std::cerr << "You should place the name of the file.\n";
		exit(1);
	}
	if (ac > 2) {
		std::cerr << "Only one parameter is allowed.\n";
		exit(1);
	}
	try {
		ParseXML tmp; //ParseXML tmp("fiieName");
		tmp.setFile(av[1]);
		auto vecOfPairs = tmp.getRes();
		PrimesSearch res(vecOfPairs); //res.calculate(vecOfPairs);
		res.output();
	}
	catch (std::exception & e) {
		std::cerr<<e.what()<<std::endl;
		return (1);
	}
	return (0);
}
