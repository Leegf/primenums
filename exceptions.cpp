#include <string>

using std::string;

class corrupted_file : public std::exception {
public:
	corrupted_file(char const * par) : _par(par) { }
	const char * what() const noexcept {return _par.c_str(); }

private:
	std::string _par;
};

class wrong_format : public std::exception {
public:
	wrong_format(char const * par) : _par(par) { }
	const char * what() const noexcept {return _par.c_str(); }

private:
	std::string _par;

};
