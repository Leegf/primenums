#include "SimpleLexer.h"

SimpleLexer::SimpleLexer() {}

SimpleLexer::~SimpleLexer() {}

/**
 * @param tmp string to check.
 * @return returns true if the buffered file(string) satisfies the proposed structure.
 */
bool SimpleLexer::checkFile(const std::string &tmp) {
	std::regex e(
			R"(<intervals>\s*(<interval>\s*<low>\s*[1-9][0-9]*\s*<\/low>\s*<high>\s*[0-9]*\s*<\/high>\s*<\/interval>\s*)*\s*<\/intervals>\s*)");

	auto textBegin = std::sregex_iterator(tmp.cbegin(), tmp.cend(), e);
	auto textEnd = std::sregex_iterator();
	return (std::distance(textBegin, textEnd) == 1);
}
