#ifndef SIMPLELEXER_H
# define SIMPLELEXER_H
#include "main.h"
#include <regex>

class SimpleLexer {
public:
	SimpleLexer();
	~SimpleLexer();
	bool checkFile(const std::string & tmp);
};

#endif
