#include "PrimesSearch.h"

PrimesSearch::PrimesSearch() {}

PrimesSearch::~PrimesSearch() {}

/**
 * @brief simply calls its other method, calculate.
 * @param par vector containing low-high pairs(intervals).
 */
PrimesSearch::PrimesSearch(std::vector<std::pair<int, int>> par) {
	calculate(std::move(par));
}

/**
 * @brief sends intervals to standalone threads to be calculated.
 * @param par vector containing low-high pairs(intervals).
 */
void PrimesSearch::calculate(std::vector<std::pair<int, int>> par) {
	for (auto & i : par)
		_futVec.push_back(std::async(std::launch::async,
	 &PrimesSearch::_searchSimpleNums, this, i.first, i.second));

}

/**
 * @brief stores all found prime numbers in interval to the shared container.
 * @param low start of the interval.
 * @param high end of the interval.
 */
void PrimesSearch::_searchSimpleNums(int low, int high) {
	auto tmp = [this](int x) -> void {
		std::lock_guard<std::mutex> lock(_m);
		_res.push_back(x);
	};
	for (int i = low; i <= high; i++) {
		if (i % 2 == 0 && i != 2) {
			continue ;
		}
		if (_isPrime(i))
			tmp(i);
	}
}

/**
 * @brief simple function to check if the num if prime.
 * @param num num to check.
 * @return true if num is prime.
 */
bool PrimesSearch::_isPrime(int num) const {
	if (num == 1)
		return true;
	for (int i = 2; i * i <= num; i++) {
		if (num % i == 0)
			return false;
	}
	return true;
}

/**
 * @brief file with name par will be created. Result of calculations will be outputed there. By default name is 'output.xml'
 * @param par name of the file to output to.
 */
void PrimesSearch::output(std::string par) {
	for (auto &i : _futVec)
		i.get();
	std::ofstream ofs;

	ofs.open(par);
	if (!ofs.is_open())
		throw corrupted_file("Can't write to the file");
	ofs<<"<root>\n<primes> ";
	for (auto & i : _res)
		ofs<<i<<" ";
	ofs<<"</primes>\n</root>\n";
}

