To build run following command: `cmake . && make`. There will be created two programs: main and test.

To build manually program: `clang++ -std=c++11 main.cpp exceptions.cpp PrimeInterval.cpp -o main`


`./main filename` to run the program itself. In case of an error it'll return 1 with the according message.

`./test` to run a set of tests. Test files listed in `tests` folder are necessary for it.

Tested on macOS 10.13.3 with clang++(Apple LLVM version 9.0.0 (clang-900.0.39.2)
)
