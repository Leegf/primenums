#include "ParseXML.h"

/**
 * @brief simply calls its other method, setFile.
 * @param filename name of the file.
 */
ParseXML::ParseXML(const char *fileName) {
	setFile(fileName);
}

ParseXML::ParseXML() {}

ParseXML::~ParseXML() {}

/**
 * @brief prepares the file passed as an argument to be parsed.
 * @param fileName name of the file.
 */
void ParseXML::setFile(const char *fileName) {
	std::regex e(R"(.*\.xml)");

	if (strlen(fileName) == 0)
		throw corrupted_file("File can't be empty");
	if (!std::regex_match(fileName, e))
		throw corrupted_file("Extension should be .xml");
	std::fstream ifs;
	std::stringstream buff_tmp;
	ifs.open(fileName);
	if (!ifs.is_open())
		throw corrupted_file("File can't be opened");
	buff_tmp << ifs.rdbuf();
	_buff = buff_tmp.str();
	_flag = true;
	ifs.close();
}

/**
 * @brief checks if file is formatted well. Can't call checkFile if the file is not set. _flag is true if the file is set.
 * @return returns true if the file is formatted well.
 */
bool ParseXML::lexerCheck() {
	if (_flag)
		return _lex.checkFile(_buff);
	else
		throw (corrupted_file("File to parse hadn't been set"));
}

/**
 * @brief parses buffered file, adding each low-high pair to the vector.
 * @return returns the vector containing all intervals as pairs.
 */
std::vector<std::pair<int, int>> ParseXML::getRes() {
	if (!lexerCheck())
		throw wrong_format("File is formatted in a wrong way");
	std::smatch m;
	int high, low;

	std::regex e_low (R"(<low>\s*)");
	std::regex e_high (R"(<high>\s*)");
	while (std::regex_search(_buff, m, e_low))
	{
		low = stoi(m.suffix());
		std::regex_search(_buff, m, e_high);
		high = stoi(m.suffix());
		if (low > high)
			throw wrong_format("Low value should be less than high");
		_res.emplace_back(std::pair<int, int>(low, high));
		_buff = m.suffix();
	}
	return _res;
}

